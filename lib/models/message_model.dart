//

import 'package:meskotis/models/user_model.dart';

class Message {
  final User sender;
  final String time;
  final String text;
  final bool isLike;
  final bool unread;

  Message({this.sender, this.time, this.text, this.isLike, this.unread});
}

//YOU - Current User

final User currentUser = User(
  id: 0,
  name: 'Current User',
  imageUrl: 'images/soso.jpg',
);

//USERS

final User soso = User(
  id: 1,
  name: 'Soso',
  imageUrl: 'images/soso.jpg',
);

final User jael = User(
  id: 2,
  name: 'Jael',
  imageUrl: 'images/femmeafrik.jpg',
);

final User mukam = User(
  id: 3,
  name: 'Mukam',
  imageUrl: 'images/tamtam.jpg',
);

final User fru = User(
  id: 4,
  name: 'Fru',
  imageUrl: 'images/oreilles.jpg',
);


//FAVOURITE GROUPS

List<User> favourites = [fru, jael, mukam, soso, fru, jael, soso];

//EXAMPLE GROUPS ON HOME SCREENS

List<Message> groups =[

Message(
sender: jael,
time: '12:23',
text: ' jael viens juste de cotiser',
isLike: true,
unread: true
),
Message(
sender: mukam,
time: '12:45',
text: ' mukam viens juste de cotiser',
isLike: true,
unread: false
),
Message(
sender: currentUser,
time: '12:23',
text: ' jael viens juste de cotiser',
isLike: true,
unread:true
),
Message(
sender: fru,
time: '12:23',
text: ' fru viens juste de cotiser',
isLike: true,
unread: false
),

];