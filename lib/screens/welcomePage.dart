import 'package:flutter/material.dart';

import 'loginPage.dart';
import 'signup.dart';

class WelcomePage extends StatefulWidget {
  WelcomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  Widget _submitButton() {
    return InkWell(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => LoginPage()));
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 13),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
           
            color: Colors.white),
        child: Text(
          'Login',
          style: TextStyle(fontSize: 23, color: Colors.black),
        ),
      ),
    );
  }

  Widget _signUpButton() {
    return InkWell(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => SignUpPage()));
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 13),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          border: Border.all(color: Colors.white, width: 2),
        ),
        child: Text(
          'Register now',
          style: TextStyle(fontSize: 23, color: Colors.white),
        ),
      ),
    );
  }

 

  Widget _title() {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
          text: 'Mes ',
          style: TextStyle(color: Colors.white, fontSize: 40, fontWeight: FontWeight.bold),
          children: [
            TextSpan(
              text: 'Cotis',
              style: TextStyle(color: Colors.red, fontSize: 40, fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text: '',
              style: TextStyle(color: Colors.white, fontSize: 30),
            ),
          ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:SingleChildScrollView(
        child:Stack(
          children: [
            Image.asset("image/main.jpeg", height: MediaQuery.of(context).size.height, width: MediaQuery.of(context).size.width,),
            Container(
                 width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 40),
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                   
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.black45.withOpacity(0.3), Colors.black87.withOpacity(0.3),])),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                     padding: EdgeInsets.only( bottom: 20.0),
                      child: _title(),
                    ),
                   
                    Padding(
                     padding: EdgeInsets.only(top: 20.0, bottom: 10.0),
                      child: _submitButton(),
                    ),
                   
                    Padding(
                      padding: EdgeInsets.only(top: 10.0, bottom: 10.0),
                      child: _signUpButton(),
                    ),
                   
                   
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }
}
