import 'package:flutter/material.dart';
import 'package:meskotis/models/message_model.dart';
import 'package:meskotis/widgets/category_selector.dart';
import 'package:meskotis/widgets/fav_groups.dart';
import 'package:meskotis/widgets/recent_groups.dart';

class privategroups extends StatefulWidget {
  @override
  _privategroupsState createState() => _privategroupsState();
}

class _privategroupsState extends State<privategroups> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.menu), color: Colors.white, onPressed: null),
        title: Center(
          child: Text('Mes Cotis',
              style: TextStyle(fontSize: 23.0, fontWeight: FontWeight.bold)
              ),
        ),
        elevation: 0.0,
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.search), color: Colors.white, onPressed: null),
          IconButton(
              icon: Icon(Icons.add), color: Colors.white, onPressed: null),
        ],
      ),
      body: Column(
        children: <Widget>[
          CategorySelector(),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Theme.of(context).accentColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30.0),
                  topRight: Radius.circular(30.0),
                ),
              ),
              child: Column(
                children: <Widget>[
                  FavGroups(),
                  RecentGroups(),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
