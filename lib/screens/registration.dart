import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'signup.dart';

class RegistrationPage extends StatefulWidget {
  RegistrationPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  File image;

  pickerCam() async {
    File img = await ImagePicker.pickImage(source: ImageSource.camera);
    if (img != null) {
      image = img;
      setState(() {});
    }
  }

  pickerGallery() async {
    File img = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (img != null) {
      image = img;
      setState(() {});
    }
  }

  Widget _backButton() {
    return InkWell(
      onTap: () {
        Navigator.pop(context);
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 0, top: 10, bottom: 10),
              child: Icon(Icons.keyboard_arrow_left, color: Colors.white),
            ),
            Text('Back',
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500))
          ],
        ),
      ),
    );
  }

  Widget _entryField(String title, {bool isPassword = false}) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(20)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 15, color: Colors.white),
          ),
          Padding(
            padding: EdgeInsets.only(top: 10),
            child: TextField(
                obscureText: isPassword,
                decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15)),
                    fillColor: Color(0xfff3f3f4),
                    filled: true)),
          )
        ],
      ),
    );
  }

  Widget _submitButton() {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(vertical: 15),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [Colors.red, Colors.red])),
      child: Text(
        'Register',
        style: TextStyle(fontSize: 20, color: Colors.white),
      ),
    );
  }

  Widget _title() {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
          text: 'Mes ',
          style: TextStyle(
              color: Colors.white, fontSize: 40, fontWeight: FontWeight.bold),
          children: [
            TextSpan(
              text: 'Cotis',
              style: TextStyle(
                  color: Colors.red, fontSize: 40, fontWeight: FontWeight.bold),
            ),
            TextSpan(
              text: '',
              style: TextStyle(color: Colors.white, fontSize: 30),
            ),
          ]),
    );
  }

  Widget _emailPasswordWidget() {
    return Column(
      children: <Widget>[
        _entryField("Name"),
        _entryField("Password", isPassword: true),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Container(
      height: MediaQuery.of(context).size.height,
      child: ListView(
        children: [
          Container(
            child: Stack(
              children: <Widget>[
                Image.asset(
                  "image/tamtam.jpeg",
                  height: double.infinity,
                  fit: BoxFit.cover,
                ),
                Center(
                  child: Container(
                    
                    width: MediaQuery.of(context).size.width * 0.9,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: Colors.black,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black54,
                            blurRadius: 10,
                            offset: Offset(10, 10),
                            spreadRadius: 0),
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          _title(),
                          Padding(
                            padding: EdgeInsets.only(top: 20),
                            child: _emailPasswordWidget(),
                          ),
                          Padding(
                              padding: const EdgeInsets.only(top: 20),
                              child: Column(
                                children: [
                                  Row(
                                    children: <Widget>[
                                      FlatButton.icon(
                                          icon: Icon(Icons.image,  color: Colors.red),
                                          label: Text("From Gallery" ,style: TextStyle(color: Colors.red)),
                                          onPressed: pickerGallery),
                                      FlatButton.icon(
                                          icon: Icon(Icons.camera_alt, color: Colors.red),
                                          label: Text("From Camera", style: TextStyle(color: Colors.red)),
                                          onPressed: pickerCam),
                                    ],
                                  ),
                                  Divider(),
                                  Container(
                                    height: 200.0,
                                    width: 200.0,
                                    decoration: new BoxDecoration(
                                      border:
                                          new Border.all(color: Colors.blueAccent),
                                    ),
                                    padding: new EdgeInsets.all(5.0),
                                    child: image == null
                                        ? Text('Select An Image', style: TextStyle(color: Colors.white))
                                        : Image.file(image),
                                  ),
                                ],
                              )),
                          Padding(
                              padding: const EdgeInsets.only(top: 20),
                              child: Column(
                                children: [
                                  Row(
                                    children: <Widget>[
                                      FlatButton.icon(
                                          icon: Icon(Icons.image),
                                          label: Text("From Gallery"),
                                          onPressed: pickerGallery),
                                      FlatButton.icon(
                                          icon: Icon(Icons.camera_alt),
                                          label: Text("From Camera"),
                                          onPressed: pickerCam),
                                    ],
                                  ),
                                  Divider(),
                                  Container(
                                    height: 200.0,
                                    width: 200.0,
                                    decoration: new BoxDecoration(
                                      border:
                                          new Border.all(color: Colors.blueAccent),
                                    ),
                                    padding: new EdgeInsets.all(5.0),
                                    child: image == null
                                        ? Text('Select An Image')
                                        : Image.file(image),
                                  ),
                                ],
                              )),
                          Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: _submitButton(),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Positioned(top: 40, left: 0, child: _backButton()),
              ],
            ),
          ),
        ],
      ),
    )));
  }
}
