import 'package:flutter/material.dart';
//import 'dart:html';
import 'package:meskotis/models/message_model.dart';
import 'package:meskotis/screens/groupe.dart';

class FavGroups extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.0),
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Groupes Favorites',
                  style: TextStyle(
                      color: Colors.blueGrey,
                      fontSize: 15.0,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1.0),
                ),
                IconButton(
                    icon: Icon(Icons.more_horiz),
                    iconSize: 30.0,
                    color: Colors.blueGrey,
                    onPressed: null)
              ],
            ),
          ),
          Container(
            height: 100.0,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                padding: EdgeInsets.only(left: 10.0),
                itemCount: favourites.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
 onTap: () => Navigator.push(context, MaterialPageRoute(builder: (_) => infoGroupe(user: favourites[index ]))),
                                      child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(bottom: 10.0, left: 10),
                          child: CircleAvatar(
                            radius: 35,
                            backgroundImage: AssetImage(favourites[index].imageUrl),
                          ),
                        ),
                        Text(favourites[index].name,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 15.0,
                                color: Colors.blueGrey)),
                      ],
                    ),
                  );
                }),
          )
        ],
      ),
    );
  }
}
